
package com.inspirethis.mike.matchterms2;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

//import com.example.matchem.R;
//import com.mjj.draggable.DragListener;
//import com.mjj.draggable.DragNDropAdapter;
//import com.mjj.draggable.DragNDropListView;
//import com.mjj.draggable.DropListener;
//import com.mjj.draggable.RemoveListener;


/**
 * Match Term Activity which allows User to drag definitions to match the terms.
 * In this case two arrayAdapters were used for listView.
 * @author Michael Jagielo
 *
 */
public class MatchEmActivity extends ListActivity {
	final Context context = this;
	Button button;
	ArrayList<String> keys;
	String[] values;
	private Button check_answers, create_matchem;
	public CheckBox dontShowAgain;
	public static final String PREFS_NAME = "MyPrefsFile1";
	private SharedPreferences settings;
    private TextView toast_text;     
    private Toast toast; 
	private View layout;
	ArrayList<MatchEm> list = new ArrayList<MatchEm>();
	Map<String, String> map;
	private boolean isUserMatchEm = false;
	public static MatchEm selected_matchem;
	private final MatchEmDBAdapter database = MatchEmDBAdapter.getInstance(MatchEmActivity.this);
	public static int listItemViewHeight = 0;
	public static int buttonHeight = 0;
	public static int temp_height = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// ContentView contains views using two separate adapters
		setContentView(R.layout.dragndroplistview);
		settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        // set listeners for buttons with anonymous classes
        addListenerOnCheckButton();
        addListenerOnAddButton();

        
        // temp hack to size button and allow the keys draggable list to
        // inflate properly
        // proper solution includes access to button.getHeight() to dynamically
        // calculate the item size in listView
        buttonHeight = (int)(check_answers.getMinimumHeight() * 1.44);
        create_matchem.setHeight(buttonHeight);
        check_answers.setHeight(buttonHeight);
        listItemViewHeight = getListItemViewHeight();
	}	

	@Override
	protected void onStart() {
		super.onStart();

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		MatchEm matchEm = extras.getParcelable("matchem");
		isUserMatchEm = extras.getBoolean("user_built");
		
		 // save reference to current matchEm for deleting in onOptionsItemSelected
		 selected_matchem = matchEm;
		// set title to the activity, to be displayed in header
		setTitle(matchEm.getTitle());

		// AlertDialog hint to User is displayed if check box was not selected
		// determined by previously stored boolean.
		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		LayoutInflater adbInflater = LayoutInflater.from(this);
		View checkBoxView = adbInflater.inflate(R.layout.checkbox, null);
		dontShowAgain = (CheckBox) checkBoxView.findViewById(R.id.skip);
	       adb.setView(checkBoxView);
	        adb.setTitle("Hint:");
	        adb.setMessage("Slide the definitions in the right column to match their " +
					"cooresponding terms on the left column... ");
	        dontShowAgain.setText("Don't show this again");
	        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	Boolean checked = false;
	                if (dontShowAgain.isChecked())
	                	checked = true;
	                Editor editor = settings.edit();
	                editor.putBoolean("boolKey", checked); 
	                editor.commit();
	                return;
	            }
	        });

	        Boolean bool = settings.getBoolean("boolKey", false);
	        if(!bool)
	            adb.show();
	    
	    // Obtain Map for selected MatchEm
	    map = new HashMap<String, String>();
		map = matchEm.getMap();	
		keys = new ArrayList<String>();
		values = new String[map.size()];
		int index = 0;
		// For each key/value pair in map, place the key in keys arrayList 
		// and the value in values string array.
		for (Map.Entry<String, String> mapEntry : map.entrySet()) {
		    keys.add(mapEntry.getKey());
		    values[index] = mapEntry.getValue();
		    index++;
		}
		// shuffle the values to be displayed
		shuffleArray(values);
        // Simple ArrayAdapter receives the values string array
        ListView lv1 = (ListView) findViewById(R.id.android_list_);
        lv1.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values));
        //temp_height = lv1.getHeight();
        Log.d("Tag", "Getting temp height (from values list) >> " + temp_height);
		// DragNDropAdapter receives the keys arrayList
		setListAdapter(new DragNDropAdapter(this, new int[]{R.layout.dragitem}, new int[]{R.id.TextView01}, keys));
        
        ListView listView = getListView();
        temp_height = lv1.getHeight();
        if (listView instanceof DragNDropListView) {
        	((DragNDropListView) listView).setDropListener(mDropListener);
        	((DragNDropListView) listView).setRemoveListener(mRemoveListener);
        	((DragNDropListView) listView).setDragListener(mDragListener);
        }

        // set up Toast for User update when checking answers
        setUpToast();
	}

	public int getStatusBarHeight() {
	      int result = 0;
	      int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
	      if (resourceId > 0) {
	          result = getResources().getDimensionPixelSize(resourceId);
	      }
	      return result;
	}
	 
	public int getTitleBarHeight() {
	    int viewTop = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
	    return Math.abs(viewTop - getStatusBarHeight());
	}
	
	public int getListItemViewHeight() {
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int overAllHeight = metrics.heightPixels;
		Log.d("Tag", "Getting title bar height >> " + getTitleBarHeight() + "Getting status bar height >> " + getStatusBarHeight() + " button height: " + buttonHeight);

		return (overAllHeight - (getTitleBarHeight() + getStatusBarHeight() + buttonHeight))/5; 
	}

	private DropListener mDropListener = 
			new DropListener() {
	        public void onDrop(int from, int to) {
	        	ListAdapter adapter = getListAdapter();
	        	if (adapter instanceof DragNDropAdapter) {
	        		((DragNDropAdapter)adapter).onDrop(from, to);
	        		getListView().invalidateViews();
	        	}
	        }
	    };
	    
	    private RemoveListener mRemoveListener =
	        new RemoveListener() {
	        public void onRemove(int which) {
	        	ListAdapter adapter = getListAdapter();
	        	if (adapter instanceof DragNDropAdapter) {
	        		((DragNDropAdapter)adapter).onRemove(which);
	        		getListView().invalidateViews();
	        	}
	        }
	    };
	    
	    private DragListener mDragListener =
	    	new DragListener() {
	    	int backgroundColor = 0xFFFF99FF;
	    	int defaultBackgroundColor;
	    	
				public void onDrag(int x, int y, ListView listView) {
				}

				public void onStartDrag(View itemView) {
					// Corrected source code example for draggable list item:
					// invalidate previous view (if any) in order to display the 
					// correct item being dragged
					itemView.invalidate();
					itemView.setVisibility(View.INVISIBLE);
					defaultBackgroundColor = itemView.getDrawingCacheBackgroundColor();
					itemView.setBackgroundColor(backgroundColor);
					ImageView iv = (ImageView)itemView.findViewById(R.id.ImageView01);
					if (iv != null) iv.setVisibility(View.INVISIBLE);
				}

				public void onStopDrag(View itemView) {
					itemView.setVisibility(View.VISIBLE);
					itemView.setBackgroundColor(defaultBackgroundColor);
					ImageView iv = (ImageView)itemView.findViewById(R.id.ImageView01);
					if (iv != null) iv.setVisibility(View.VISIBLE);
				}
	    };
	    
	    @Override
		public boolean onPrepareOptionsMenu(Menu menu) {
	    	menu.clear();
			MenuInflater inflater = getMenuInflater();
			if (isUserMatchEm)
				inflater.inflate(R.menu.items, menu);
			else
				inflater.inflate(R.menu.main, menu);
			return super.onPrepareOptionsMenu(menu);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
	       // Handle action buttons
	       switch(item.getItemId()) {
	       case R.id.action_websearch:
	           // create intent to perform web search for this planet
	           Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
	           intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
	           // catch event that there's no activity to handle intent
	           if (intent.resolveActivity(getPackageManager()) != null) {
	               startActivity(intent);
	           } else {
	               Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
	           }
	           return true;
	       case R.id.delete:		   
	    		   // AlertDialog checkDelete
	   		AlertDialog.Builder adb = new AlertDialog.Builder(this);
	   		//LayoutInflater adbInflater = LayoutInflater.from(this);
	   	        adb.setTitle("Attention:");
	   	        adb
	   	        
	   	        .setMessage("Are you sure that you would like to delete this Match Term List?")
	   	        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	   	            public void onClick(DialogInterface dialog, int which) {
	   	            	Editor editor;
	   	            	
	   	            	// solution to avoid deleting from the middle of arrayList
	   	            	// clear list and reload it from database in MainActivity.
	   	            	if (UserBuiltMatchEms.getMatchEMs().size() == 1){
					        editor = settings.edit();
					        // set to false as size = 1, and deleting last one.
					        editor.putBoolean("boolKeyAdded", false); 
					        editor.commit();
	   	            	}
	   	            	database.open();
	   	            	database.deleteMatchEm(selected_matchem);
	   	            	database.close();
				        
	   	            	editor = settings.edit();
				        editor.putBoolean("boolKeyDeleted", true); 
				        editor.commit();
				        // clear list here then rebuild conditionally in MainActivity
				        UserBuiltMatchEms.clearMatchEmList();
	   	            	startActivity(new Intent(MatchEmActivity.this, MainActivity.class)
	   	            	.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
	   	            }
	   	        	})
	   	            
	   	        .setNegativeButton("No", new DialogInterface.OnClickListener() {
	   	        	 public void onClick(DialogInterface dialog,int id) {
	   	        		 dialog.cancel();
	   	        	 }   
	   	        });
	   	        
	   	        AlertDialog alertDialog = adb.create();
	   	     alertDialog.show();
	       default:
	           return super.onOptionsItemSelected(item);
	       }
		}
	
    private void addListenerOnCheckButton() {
    	check_answers = (Button) findViewById(R.id.check_button);
    	//buttonHeight = check_answers.getMeasuredHeight();
    	check_answers.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String[] temp = keys.toArray(new String[list.size()]);
				if (temp.length < 5) {
					// this case is for if User has not built any MatchEms yet
					//TODO: perhaps add this to the list view if matchem_str is length = 0 || null.
					Toast.makeText(getBaseContext(), "You can build your own Match Term by clicking \"Create your own\" on left from an active MatchTerms Activity..." ,
							Toast.LENGTH_LONG).show();
				} else if(checkAnswers(temp,values)){
	    	    	toast_text.setText("Great Job! You got them all correct!" );
					toast.show();  
				}
				else
					Toast.makeText(getBaseContext(), "Try again..." ,
							Toast.LENGTH_LONG).show();
    	}
    	
    	});
    }
    	
        private void addListenerOnAddButton() {
        	create_matchem = (Button) findViewById(R.id.add_button);
        	create_matchem.setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				startActivity(new Intent(MatchEmActivity.this, CreateMatchEmsActivity.class).
    						addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)); 
    			}
        	});
    }
    
    private void shuffleArray(String[] str) {
        int n = str.length;
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
          int change = i + random.nextInt(n - i);
          swap(str, i, change);
        }
      }
    
    private static void swap(String[] str, int i, int change) {
        String helper = str[i];
        str[i] = str[change];
        str[change] = helper;
      }

    private boolean checkAnswers(String[] keys, String[] vals) {
    	int n = keys.length;
    	
    	for (int i = 0; i < n; i++) {
    		if (!map.get(keys[i]).equals(vals[i]))
    			return false;
    	}
    	return true;
    }
    
	private void setUpToast() {
		toast = new Toast(this);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
        layout = inflater.inflate(R.layout.toast, null);
        toast_text = (TextView) layout.findViewById(R.id.text); 
        toast.setDuration(Toast.LENGTH_LONG);        
        toast.setView(layout);
	}

	@Override
	public void onBackPressed() {
		// go back to MainActivity, remove this activity from stack
		startActivity(new Intent(MatchEmActivity.this, MainActivity.class).
				addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)); 
	}
    
}

