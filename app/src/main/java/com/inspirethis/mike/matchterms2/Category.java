package com.inspirethis.mike.matchterms2;

import java.util.ArrayList;
/**
 * Category class for Match Term Application
 * 
 * @author Michael Jagielo
 *
 */
public class Category {
	public static int next_id = 1;
	private String name;
	private int id;
	private ArrayList<MatchEm> MatchEMsList;
	
	public Category(String name) {
		setName(name);
		MatchEMsList = new ArrayList<MatchEm>();
        id = next_id;
        next_id++;
	}
	
	public String[] getNameList(final ArrayList<MatchEm> matchem_list) {
		String[] name_list = new String[matchem_list.size()]; 
		
		for (int i = 0; i < name_list.length; i++) {
			name_list[i] = matchem_list.get(i).getTitle();
		}
		return name_list;
	}
	
	public void addMatchEM(final MatchEm the_matchem) {
		MatchEMsList.add(the_matchem);
	}
	
	public void addAllMatchems(ArrayList<MatchEm> list) {
		MatchEMsList = list;
	}
	
	public ArrayList<MatchEm> getMatchEMs() {
		return MatchEMsList;
	}
	
	public MatchEm getMatchEm(final String the_name) {
		MatchEm m = null;
		for (MatchEm i : MatchEMsList) {
			if (i.getTitle().equals(the_name)) {
				m = i;
			}
		}
		return m;
	}
	
	public MatchEm getMatchEm(final int the_id) {
		MatchEm m = null;
		for (MatchEm i : MatchEMsList) {
			if (i.getID() == the_id) {
				m = i;
			}
		}
		return m;
	}
	
	@Override
	public boolean equals(final Object the_obj) {
		if (the_obj == null || !(the_obj instanceof Category))
			return false;
		return (((Category)the_obj).getName().equals(this.getName())); 
				
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
    public int getID() {
        return id;
    }

}
