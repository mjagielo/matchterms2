package com.inspirethis.mike.matchterms2;

import java.util.ArrayList;

/**
 * The UserBuiltMatchEms class was created to handle the data representation of the dynamic
 * content from User. 
 *  
 * @author Michael
 *
 */
public class UserBuiltMatchEms {
	
	private static String name = "My Match Terms";
	private int id;
	// store all newly created MatchEms and maintain only one instance of this class
	private static ArrayList<MatchEm> MatchEMsList = new ArrayList<MatchEm>();
	
	private static volatile Category instance =
			new Category("My Match Terms");
	
	private UserBuiltMatchEms(){}
	
	public static Category getInstance() {
		return instance;
	}
	
	public static void setMatchEmList(final ArrayList<MatchEm> the_list) {
		MatchEMsList = the_list;
	}
	
	public static void clearMatchEmList() {
		MatchEMsList.clear();
	}
	
	public static String[] getNameList(final ArrayList<MatchEm> matchem_list) {
		String[] name_list = new String[matchem_list.size()]; 
		
		for (int i = 0; i < name_list.length; i++) {
			name_list[i] = matchem_list.get(i).getTitle();
		}
		return name_list;
	}
	
	public static void addMatchEM(final MatchEm the_matchem) {
		MatchEMsList.add(the_matchem);
	}
	
	public void addAllMatchems(ArrayList<MatchEm> list) {
		MatchEMsList = list;
	}
	
	public static ArrayList<MatchEm> getMatchEMs() {
		return MatchEMsList;
	}
	
	public static MatchEm getMatchEm(final String the_name) {
		MatchEm m = null;
		for (MatchEm i : MatchEMsList) {
			if (i.getTitle().equals(the_name)) {
				m = i;
			}
		}
		return m;
	}

	public MatchEm getMatchEm(final int the_id) {
		MatchEm m = null;
		for (MatchEm i : MatchEMsList) {
			if (i.getID() == the_id) {
				m = i;
			}
		}
		return m;
	}
	
	@Override
	public boolean equals(final Object the_obj) {
		if (the_obj == null || !(the_obj instanceof Category))
			return false;
		return (((Category)the_obj).getName().equals("My Match Terms")); 
				
	}
	
	public static String getName() {
		return name;
	}

    public int getID() {
        return id;
    }
}
