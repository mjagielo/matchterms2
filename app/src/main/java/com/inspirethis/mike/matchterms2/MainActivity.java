package com.inspirethis.mike.matchterms2;


/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

//import com.crittercism.app.Crittercism;
//import com.example.matchem.R;


/**
 * Main Activity using Navigation Bar, much of which was derived from Android Developer site
 * used as an exercise.
 * The code for listView getItem etc. will be revised in version 2.0 when content is
 * download-able from server. The static content will not be needed and database will
 * be used for all data.
 * @author Michael Jagielo
 *
 */
public class MainActivity extends Activity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    public static ArrayList<Category> category_list = new ArrayList<Category>();
    private ArrayList<MatchEm> my_matchem_list = new ArrayList<MatchEm>();
    private static String[] mSubjectTitles;
    private CheckBox dontShowAgain;
    public static final String PREFS_NAME = "MyPrefsFile1";
    private SharedPreferences settings;
    Boolean matchemDeleted, matchemAdded;
    private final MatchEmDBAdapter database = MatchEmDBAdapter.getInstance(MainActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: download latest Crittercism jar and add to project
        //Crittercism.initialize(getApplicationContext(), "525df373d0d8f737c800000b");
        setContentView(R.layout.activity_main);
        settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        matchemAdded = settings.getBoolean("boolKeyAdded", false); // defaults to false is value DNE.
        matchemDeleted = settings.getBoolean("boolKeyDeleted", false);

        category_list = buildStaticData();
        // +1 below to add space for "My Match Terms" element
        mSubjectTitles = new String[category_list.size()+1];

        mTitle = mDrawerTitle = getTitle();
        mSubjectTitles = getCategoryNames(category_list);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mSubjectTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
        // this will run if a deletion has occurred (UserBuiltMatchEms is cleared
        // in MatchEmActivity) and a MatchEm still exists in db (matchemAdded == true)
        // also runs at start up when a MatchEm is in database (UserBuiltMatchEms
        // always empty at startup).
        if(matchemAdded && UserBuiltMatchEms.getMatchEMs().isEmpty()) {
            // clear out any previous items in list
            my_matchem_list.clear();
            database.open();
            my_matchem_list = database.getAllMatchEms(UserBuiltMatchEms.getName());
            database.close();
//TODO: check if: UserBuiltMatchEms.setMatchEmList(my_matchem_list); works
            for (MatchEm m : my_matchem_list)
                UserBuiltMatchEms.addMatchEM(m);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
                // catch event that there's no activity to handle intent
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = new SubjectFragment();
        Bundle args = new Bundle();
        args.putInt(SubjectFragment.ARG_SUBJECT_NUMBER, position);
        fragment.setArguments(args);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mSubjectTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggle
        mDrawerToggle.onConfigurationChanged(newConfig);
    }



    /**
     * Fragment that appears in the "content_frame", shows a listview
     */
    public static class SubjectFragment extends ListFragment {
        public static final String ARG_SUBJECT_NUMBER = "subject_number";
        private String subject;
        private Category cat_temp;
        private ArrayList<Category> cat_list = new ArrayList<Category>();
        private String[] matchem_str = null;

        public SubjectFragment() {
            // Empty constructor required for fragment subclasses
        }

        public interface MatchEmListViewListener {
            public boolean onMatchEmSelected(final View the_view, final MatchEm the_matchem);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            cat_list = category_list;
            // get id of list item selected
            int i = getArguments().getInt(ARG_SUBJECT_NUMBER);
            // retrieve item from stored array of category names...
            subject = getResources().getStringArray(R.array.subjects_array)[i];
            // for each item in our category list, check if name equals our selected name
            // it would be better to simply fetch category by id here...
            for(Category c : cat_list) {
                // determine if the category is other that user_matchems
                if (subject.equals(c.getName()) && !c.getName().equals(UserBuiltMatchEms.getName())) {

                    // get MatchEm String array  of MatchEm names to display
                    matchem_str = c.getNameList(c.getMatchEMs());
                    // temp category variable holds the selected Category
                    cat_temp = c;
                }
                // for the case where "My Match Terms" was selected
                if (subject.equals(UserBuiltMatchEms.getName())) {
                    // matchem_str now holds the names array of latest created MatchEms
                    // note: if no User built matchEms have been built, an empty list is returned by call below
                    matchem_str = UserBuiltMatchEms.getNameList(UserBuiltMatchEms.getMatchEMs());
                    if (matchem_str.length == 0) {
                        // custom toast that runs briefly:
                        // when  standard toast is called from this location, the timer
                        // does not start until User exits List Fragment, custom thread
                        // overrides this sticky behavior.
                        final Toast toast = Toast.makeText(getActivity(), "No User Built Match Terms to display...", Toast.LENGTH_SHORT);
                        toast.show();

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                toast.cancel();
                            }
                            // let toast run for 2 seconds
                        }, 2000);
                    }
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, matchem_str);
            setListAdapter(adapter);
        }

        /*
         * Then off to MatchEMActivity with the correct matchem to display.
         * (non-Javadoc)
         * @see android.app.ListFragment#onListItemClick(android.widget.ListView, android.view.View, int, long)
         */
        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            if(subject.equals(UserBuiltMatchEms.getName())){
                MatchEm m = UserBuiltMatchEms.getMatchEm(matchem_str[position]);
                Intent intent = new Intent(this.getActivity(),
                        MatchEmActivity.class);
                Bundle extras = new Bundle();
                extras.putParcelable("matchem", m);
                extras.putBoolean("user_built", true);
                intent.putExtras(extras);
                startActivity(intent);
            } else {
                MatchEm m = cat_temp.getMatchEm(matchem_str[position]);
                Intent intent = new Intent(this.getActivity(),
                        MatchEmActivity.class);
                intent.putExtra("matchem", m);
                intent.putExtra("user_built", false);
                startActivity(intent);
            }
        }
    } // end of static class

    public String[] getCategoryNames(ArrayList<Category> list) {
        String[] str_array = new String[list.size()+1];
        int i = 0;
        for (Category c : list) {
            str_array[i] = c.getName();
            i++;
        }
        // fill last element of array with "My Match Terms"
        str_array[list.size()] = UserBuiltMatchEms.getName();
        return str_array;
    }


    public MatchEm buildMatchEM(String cat, String name, String[] key_val) {
        MatchEm matchem;

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(key_val[0], key_val[1]);
        map.put(key_val[2], key_val[3]);
        map.put(key_val[4], key_val[5]);
        map.put(key_val[6], key_val[7]);
        map.put(key_val[8], key_val[9]);

        matchem = new MatchEm(cat, name, map);

        return matchem;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDrawerLayout.openDrawer(mDrawerList);
    }

    public ArrayList<Category> buildStaticData() {
        ArrayList<Category> cat_list = new ArrayList<Category>();
        Category history = new Category("History");
        cat_list.add(history);
        Category math = new Category("Math");
        cat_list.add(math);
        Category science = new Category("Science");
        cat_list.add(science);
        Category environment = new Category("Environment");
        cat_list.add(environment);
        Category technology = new Category("Technology");
        cat_list.add(technology);
        Category cur_events = new Category("Current Events");
        cat_list.add(cur_events);
        Category english = new Category("English");
        cat_list.add(english);
        Category sports = new Category("Sports");
        cat_list.add(sports);
        Category entertainment = new Category("Entertainment");
        cat_list.add(entertainment);

        String[] history_1 = {"Battle of Monmouth", "Molly Pitcher", "Fought in the Revolutionary War", "Deborah Sampson", "Sneak attack on British, Christmas Eve", "George Washington", "Midnight Ride April 7, 1775", "Paul Revere", "set the stage for African-American women speakers", "Maria W. Stewart"};
        String[] history_2 = {"Crowned King of Punjab", "Ranjit Singh", "Leader of movement to abolish slave trade", "William Wilberforce", "Pride and Prejudice", "Jane Austen", "Exiled to Elba", "Napoleon", "Invented the Flintlock Revolver", "Elisha Haydon Collier"};
        String[] history_3 = {"Father of modern physics", "Albert Einstein", "Invented the first laser", "Theodore Maiman", "Father of the Green Revolution", "Norman Borlaug", "Decrypted German ciphers", "Alan Turing", "Free Software Foundation / GNU project", "Richard Stallman"};
        String[] history_4 = {"Antibiotics", "Louis Pasteur", "Calculus", "Isaac Newton", "Frozen Food", "Clarence Birdseye", "Bifocal eyeglasses", "Benjamin Franklin", "Electric Motor (alternating current)", "Nikola Tesla"};
        String[] history_5 = {"was declined on his offer to sell patent rights to Western Union for 100k", "Alexander Graham Bell", "failed 1000 times before realizing success in inventing light bulb", "Thomas Edison", "first spoke: age 4, learned to read: age 7, thought to be mentally handicapped", "Albert Einstein", "rejected on offer to sell company to Excite for 1 mil. in '98", "Larry Page & Sergey Brin", "early business attempts left him broke 5 times before finding success", "Henry Ford"};

        String[] math_1 = {"n(n+1)/2", "Carl Friedrich Gauss", "imaginary numbers", "Leonhard Euler", "F0=0, F1=1, Fn=Fn-1+Fn-2 for n>=2", "Fibonacci", "Proved Fermat's Last Theorem", "Andrew Miles", "a^2 + b^2 = c^2", "Pythagoras"};
        String[] math_2 = {"tiny change in value of variable (dx or dy)", "Differential", "line/curve that approaches increasingly closer", "Asymptote", "used to find derivative of composition", "Chain Rule", "Limits, derivatives, definite integrals", "Calculus", "Approach a finite limit", "Converge"};
        String[] math_3 = {"(i,j): number of edges from vertex i to j", "Adjacency Matrix", "having vertex partition into (at most) two independent sets", "Bipartite Graph", "Closed trail containing every edge", "Eulerian circuit", "Intersection is empty set", "Disjoint", "Contains all elements joined sets", "Union"};

        String[] science_1 = {"protein that speeds up reaction", "Enzyme", "molecule that has gained or lost one or more electrons", "Ion", "specializing in reaction rates", "Kinetics", "neutral unit that has no net charge", "Neutron", "carrier of electromagnetic radiation of all wavelength", "Photon"};
        String[] science_2 = {"highest rank of coal", "Anthracite", "fossil resin or tree sap, appreciated for its color", "Amber", "chain or cluster of islands", "Archipelago", "sedimentary carbonate rock, composed of calcium magnesium carbonate", "Dolomite", "the largest unit of geologic time", "Eon"};
        String[] science_3 = {"organism with a simple single-chambered stomach", "Monograstric", "phenomenon where individual animals appear outside normal range", "Vagrancy", "process of growing older, aging and physical degradation", "Catabiosis", "organism that once was abundant in large area, now occuring in only few small areas", "Relict", "system's property to regulate internal environment", "Homeostasis"};

        String[] envir_1 = {"how many tons of oil produced in the world each ear ends up in the ocean?", "5 million", "A typical computer monitor contains how much lead?", "4-5 pounds", "Every ton of paper that is recycled saves how many trees?", "seventeen", "Every day how many species of plants/animals become extinct as habitat is detroyed?", "50 to 100", "How much of the world's rainforest has already been lost?", "over 50%"};
        String[] envir_2 = {"amount of plastic bottles that are thrown away could circle the earth this many times", "four", "percentage of energy saved when plastic is recycled rather than made from raw materials (gas & oil)", "eighty-eight", "recycling one aluminum can save enough energy to run a computer for how many hours?", "three", "75% of waste is recyclable, but we only recycle what percent?", "thirty", "if each American recycled 1/10 of newspapers, how many million trees be saved?", "twenty-five"};
        String[] envir_3 = {"leading cause of global warming", "greenhouse effect", "change in weather patterns, warmer conditions, rising ocean levels", "effects:", "industrial activities that our modern civilization has depended upon", "increases CO2 levels:", "water vapor, carbon dioxide, methane, nitrous oxide, Chlorofluorocarbons (CFCs)", "creates greenhouse", "sun's energy output, which has been slightly decreasing", "solar irradiance"};

        String[] tech_1 = {"smallest unit of data computer can process", "bit", "program that organizes data in rows and columns", "spreadsheet", "made up of 1's and 0's", "binary numbers", "tool that finds online databases based on criteria specified by user", "search engine", "eight binary digits", "byte"};
        String[] tech_2 = {"number of computer languages in use", "2000", "stores data for a short amount of time, typically in computer memory", "buffer", "network for communication within an organization", "intranet", "how many megahertz are in a gigahertz:", "1000", "rate at which a processor can complete a processing cycle", "clock speed"};
        String[] tech_3 = {"scientists have recently created device with wood fiber to do what?", "nano battery", "one billion of these fits into one meter:", "nanometer", "nanoscale bits of metal oxide, carbon fiber, or metal blends can be used for: ", "detoxify hazardous waste", "fluorescent nanoparticles have been designed to: ", "make tumors visible", "Researchers can control the spin of a single electron by applying voltage to build what?", "quantum computer"};

        String[] cur_evt_1 = {"sky diver who broke the sound barrier with a 24 mile leap", "Felix Baumgartner", "President Obama's Republican opponent in the 2012 election:", "Mitt Romney", "followed Hillary Clinton as U.S. secretary of state:", "John Kerry", "CEO of Yahoo, wrote book: \"Lean In\"", "Marissa Mayer", "Duchess of Cambridge", "Kate Middleton"};
        String[] cur_evt_2 = {"cool science guy recently starred on Dancing with the Stars", "Bill Nye", "The Age Of Adaline", "Blake Lively", "Gravity", "Sandra Bullock", "The Perfect Storm", "George Clooney", "The Family", "Robert De Niro"};
        String[] cur_evt_3 = {"wrote: \"Russians deserve better than Putin\"", "John McCain", "wrote in Washington Post: \"to engage in constructive interaction with world\"", "Hassan Rouhani", "country where protesters have rallied for weeks against gov't.", "Bulgaria", "country where engineers righted cruise ship: Costa Concordia", "Italy", "which country's king recently announced welfare state of last century is over", "The Netherlands"};

        String[] eng_1 = {"kinder or softer term in place of one that is harsh or too straight-forward", "euphemism", "clearly expressed in few words, concise", "succint", "super-abundance; excess; more than enough", "plethora", "feeling of anger brought on by something mean or unjust", "indignant", "brief and to the point; concise", "terse"};
        String[] eng_2 = {"fear of chins", "geniophobia", "fear of falling down stairs", "climacophobia", "fear of staying single", "anuptaphobia", "fear of strangers", "xenophobia", "fear of small insects that cause itching", "acarophobia"};
        String[] eng_3 = {"branch of philosophy dealing with values, as those of ethics, aesthetics or religion", "axiology", "executed on a very small scale to create computer chips and other microscopic devices", "nanotechnology", "study of annual rings of trees in determining chronological order of past events", "dendrochronology", "science of history of words, tracing their origin and primitive significance", "etymology", "ecology dealing with an organism or species in relation to its environment", "autecology"};

        String[] sports_1 = {"most career interceptions", "Brett Favre", "highest career passing TD percentage", "Sid Luckman", "held simultaneous records: Passing Yards,  Passing TD's, Completions and Attempts", "Warren Moon", "holds NFL Rookie Record for Pass to Interception Ratio", "Russell Wilson", "3rd round NFL draft pick, broke 5 records", "Fran Tarkenton"};
        String[] sports_2 = {"holds record for highest slugging percentage", "Babe Ruth", "highest batting average, most steals home", "Ty Cobb", "most hits in a season", "Ichiro Suzuki", "longest consecutive hitting streak", "Joe DiMaggio", "most runs scored in career", "Rickey Henderson"};
        String[] sports_3 = {"fastest marathon time", "Geoffrey Mutai", "fastest 100 meter", "Usain Bolt", "fastest 40 yard in NFL", "Bo Jackson", "tied record for the longest field goal in 2011", "Sebastian Janikowski", "recently kicked a field goal in high school 4 yards longer than NFL record", "Austin Rehkow"};

        String[] ent_1 = {"Bus Stop", "The Hollies", "Louie Louie", "The Kingsmen", "I Heard it Through the Grapevine", "Marvin Gaye", "Sunshine of Your Love", "Cream", "See Emily Play", "Pink Floyd"};
        String[] ent_2 = {"Mirrors", "Justin Timberlake", "Peace & Quiet", "Waxahatchee", "Weight", "Mikal Cronin", "Retrograde", "James Blake", "Can't Hold Us", "Macklemore, et al."};
        String[] ent_3 = {"The Cure for Insomnia", "Longest Movie", "My Big Fat Greek Wedding", "Most Profitable", "Lord of the Rings", "Most Awards", "An Inconvenient Truth", "By Al Gore", "Inside Job", "Matt Damon"};


        history.addMatchEM(buildMatchEM(history.getName(), "Early American", history_1));
        history.addMatchEM(buildMatchEM(history.getName(), "19th Century", history_2));
        history.addMatchEM(buildMatchEM(history.getName(), "20th Century", history_3));
        history.addMatchEM(buildMatchEM(history.getName(), "Inventions", history_4));
        history.addMatchEM(buildMatchEM(history.getName(), "Rejections", history_5));
        math.addMatchEM(buildMatchEM(math.getName(), "Famous Mathematicians", math_1));
        math.addMatchEM(buildMatchEM(math.getName(), "Calculus", math_2));
        math.addMatchEM(buildMatchEM(math.getName(), "Discrete Math", math_3));
        science.addMatchEM(buildMatchEM(science.getName(), "Chemistry", science_1));
        science.addMatchEM(buildMatchEM(science.getName(), "Geology", science_2));
        science.addMatchEM(buildMatchEM(science.getName(), "Biology", science_3));
        environment.addMatchEM(buildMatchEM(environment.getName(), "Ecology", envir_1));
        environment.addMatchEM(buildMatchEM(environment.getName(), "Recycling", envir_2));
        environment.addMatchEM(buildMatchEM(environment.getName(), "Global Warming", envir_3));
        technology.addMatchEM(buildMatchEM(technology.getName(), "Tech Terms", tech_1));
        technology.addMatchEM(buildMatchEM(technology.getName(), "Computing", tech_2));
        technology.addMatchEM(buildMatchEM(technology.getName(), "Nano", tech_3));
        cur_events.addMatchEM(buildMatchEM(cur_events.getName(), "Who's in the News?", cur_evt_1));
        cur_events.addMatchEM(buildMatchEM(cur_events.getName(), "Hollywood", cur_evt_2));
        cur_events.addMatchEM(buildMatchEM(cur_events.getName(), "What's the Story?", cur_evt_3));
        english.addMatchEM(buildMatchEM(english.getName(), "Vocabulary", eng_1));
        english.addMatchEM(buildMatchEM(english.getName(), "Phobias", eng_2));
        english.addMatchEM(buildMatchEM(english.getName(), "\"ology\" Words", eng_3));
        sports.addMatchEM(buildMatchEM(sports.getName(), "Football Records", sports_1));
        sports.addMatchEM(buildMatchEM(sports.getName(), "Baseball Records", sports_2));
        sports.addMatchEM(buildMatchEM(sports.getName(), "More Records", sports_3));
        entertainment.addMatchEM(buildMatchEM(entertainment.getName(), "Hits of the 60's", ent_1));
        entertainment.addMatchEM(buildMatchEM(entertainment.getName(), "Hits of 2013", ent_2));
        entertainment.addMatchEM(buildMatchEM(entertainment.getName(), "Movies", ent_3));

        return cat_list;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        LayoutInflater adbInflater = LayoutInflater.from(this);
        View checkBoxView = adbInflater.inflate(R.layout.checkbox, null);
        dontShowAgain = (CheckBox) checkBoxView.findViewById(R.id.skip);
        adb.setView(checkBoxView);
        adb.setTitle("Attention:");
        adb.setMessage("Are you sure that you would like to exit Match Terms?");
        dontShowAgain.setText("Don't show this again");
        adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                Boolean checked = false;
                if (dontShowAgain.isChecked())
                    checked = true;
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("boolKeyExit", checked);
                editor.commit();

                MainActivity.this.finish();
                return;
            }
        });
        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Boolean checked = false;
                if (dontShowAgain.isChecked())
                    checked = true;
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("boolKeyExit", checked);
                editor.commit();
                return;
            }
        });

        Boolean bool = settings.getBoolean("boolKeyExit", false);
        if(!bool)
            adb.show();
        else {
            MainActivity.this.finish();
        }
    }
}


