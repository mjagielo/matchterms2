package com.inspirethis.mike.matchterms2;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * As a fun exercise, I choose to use Parcelable at this point, which wont be needed
 * when database is used for all data.
 * @author Michael Jagielo
 *
 */
public class MatchEm implements Parcelable{
	/**
	 * 
	 */
	
	public static int next_id = 1;
	private HashMap <String, String> map = new HashMap<String, String>();
	//private Category category;
	private String category_name;
	private String title;
	//private boolean completed;
	private int id;
	private String map_str;
	private boolean is_completed;
	
	
	
	public MatchEm() {
		
	}
    public MatchEm(Parcel in ) {
        readFromParcel( in );
    }

	public MatchEm(String the_category, String the_title, HashMap<String, String> map) {
		setMap(map);
		//setCategory(category);
		setTitle(the_title);
        id = next_id;
        next_id++;
        is_completed = false;
	}
	
	
	public MatchEm(final String the_category, final String the_title, final String map) {
		setMap_str(map);
		//setCategory(the_category);
		setTitle(the_title);
        id = next_id;
        next_id++;
        is_completed = false;
	}

	public HashMap <String, String> getMap() {
		return map;
	}

	public void setMap(HashMap <String, String> map) {
		this.map = map;
	}

	public void setCategory(String the_category) {
		this.category_name = the_category;
	}
	
    public String getCategoryName() {
    	return category_name;
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String the_title) {
		this.title = the_title;
	}
	
    public void setId(final int the_id) {
        id = the_id;
    }

    public void setNextMatchEmId(final int the_id) {
        next_id = the_id;
    }
	
    public int getID() {
        return id;
    }
    
    public int getNextID() {
    	return next_id;
    }

	public void setMap_str(String map_str) {
		this.map_str = map_str;
	}
	
	public String getMapString() {
		return this.map_str;
	}

	@Override
	public int describeContents() {
		return 0;
	}

    @SuppressWarnings("rawtypes")
	public static final Creator CREATOR = new Creator() {
        public MatchEm createFromParcel(Parcel in ) {
            return new MatchEm( in );
        }
        
        public MatchEm[] newArray(int size) {
            return new MatchEm[size];
        }
    };


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(map.size());
		for (String key : map.keySet()) {
			dest.writeString(key);
			dest.writeString(map.get(key));
		}
		dest.writeString(category_name);
		dest.writeString(title);
		dest.writeInt(id);
	}
	
    private void readFromParcel(Parcel in ) {
    	int size = in.readInt();
    	for(int i = 0; i < size; i++) {
    		String key = in.readString();
    		String value = in.readString();
    		map.put(key, value);
    	}
    	category_name = in.readString();
    	title = in.readString();
    	id = in.readInt();
    }
    
    public void setIsCompleted(final boolean the_value) {
        is_completed = the_value;
    }
    
    public boolean isCompleted() {
        return is_completed;
    }

}
