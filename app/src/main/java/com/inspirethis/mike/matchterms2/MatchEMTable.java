package com.inspirethis.mike.matchterms2;

/**
 * MatchEm Table for database.
 * @author Michael Jagielo
 *
 */
public class MatchEMTable {
	public static final String MATCHEM_ROW_ID = "_id";
	public static final String MATCHEM_TITLE = "MatchEmTitle";
	public static final String MATCHEM_CATEGORY = "MatchEmCategory";
	public static final String MATCHEM_MAP = "MatchEmMap";
	public static final String MATCHEM_ID = "MatchEmID";
	public static final String MATCHEM_NEXT_ID = "NextMatchEmID";
	public static final String MATCHEM_T = "MatchEms";
	public static final String MATCHEM_COMPLETE = "MatchEmComplete";
}
