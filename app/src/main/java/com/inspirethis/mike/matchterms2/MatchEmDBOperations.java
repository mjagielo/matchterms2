package com.inspirethis.mike.matchterms2;

import java.util.ArrayList;

/**
 * Database operations interface.
 * @author Michael Jagielo
 *
 */
public interface MatchEmDBOperations {
	long addMatchEm(MatchEm match_em);
	MatchEm getMatchEm(final int id);
	ArrayList<MatchEm> getAllMatchEms(final String category_name);
	ArrayList<MatchEm> getAllMatchEms();
	void deleteMatchEm(final MatchEm the_matchem);
}
