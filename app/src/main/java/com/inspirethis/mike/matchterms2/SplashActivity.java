package com.inspirethis.mike.matchterms2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Splash Activity will be more useful when server implementation is complete
 * in version 2.0.
 * @author Michael Jagielo
 *
 */
public class SplashActivity extends Activity {
	ImageView image;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        image = (ImageView)findViewById(ImageView.generateViewId());
        
        startAnimating();        
    }
    
    /**
     * Helper method to start animation on splash screen
     */
    private void startAnimating(){
    	
    	 // Fade in top title
        TextView logo1 = (TextView) findViewById(R.id.TextViewTitle);
        Animation fade1 = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        logo1.startAnimation(fade1);
              
        
        // Fade in bottom title after a built-in delay.
        TextView logo2 = (TextView) findViewById(R.id.TextViewBottomTitle);
        Animation fade2 = AnimationUtils.loadAnimation(this, R.anim.fade_in2);
        logo2.startAnimation(fade2);
        // Transition to Main Menu when bottom title finishes animating
        fade2.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                // The animation has ended, transition to the Main Menu screen
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                SplashActivity.this.finish(); //pops splash off stack
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        
    }

        public void onClick(View v) {
        	finish();
        	android.os.Process.killProcess(android.os.Process.myPid());
        }
        
        @Override
        protected void onPause(){
        	super.onPause();
        	// stop the animation
        	TextView logo1 = (TextView) findViewById(R.id.TextViewTitle);
        	logo1.clearAnimation();
        	TextView logo2 = (TextView) findViewById(R.id.TextViewBottomTitle);
        	logo2.clearAnimation();
        }
        
        @Override
        protected  void onResume() {
        	super.onResume();        	
        	startAnimating();
        }

}
