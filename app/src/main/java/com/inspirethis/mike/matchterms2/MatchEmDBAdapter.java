package com.inspirethis.mike.matchterms2;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Database adapter with helper methods. 
 * @author Michael Jagielo
 *
 */
public class MatchEmDBAdapter implements MatchEmDBOperations {
	public static final String TAG = "MatchEmDBAdapter";
	private static final String DATABASE_NAME = "me_data.db";
	// Toggle this number for updating tables and database.
	private static final int DATABASE_VERSION = 1;
	private static MatchEmDBAdapter database_instance;
	
	//Database open/upgrade helper.
	private MyDBHelper database_helper;
	//Context of the application using the database.
	private final Context context;
	//Variable to hold the database instance.
	private static SQLiteDatabase database;
	
	public static MatchEmDBAdapter getInstance(final Context the_context) {
		if (database_instance == null)
			database_instance = new MatchEmDBAdapter(the_context);
		return database_instance;
	}
	
	private MatchEmDBAdapter(Context the_context) {
		context = the_context;
		database_helper = new MyDBHelper(context);
	}
	
	public MatchEmDBAdapter open() {
		database_helper = new MyDBHelper(context);
		try {
			database = database_helper.getWritableDatabase();
		}catch(SQLiteException the_ex) {
			database = database_helper.getReadableDatabase();
		}
		return this;
	}
	
	public void close() {
		database_helper.close();
	}
	
	@Override
	public long addMatchEm(MatchEm match_em) {
		ContentValues bucket = new ContentValues();
		bucket.put(MatchEMTable.MATCHEM_TITLE, match_em.getTitle());
		bucket.put(MatchEMTable.MATCHEM_CATEGORY, "My Match Terms");
		bucket.put(MatchEMTable.MATCHEM_MAP, new JSONObject(match_em.getMap()).toString());
		bucket.put(MatchEMTable.MATCHEM_ID, match_em.getID());
		bucket.put(MatchEMTable.MATCHEM_NEXT_ID, match_em.getNextID()); 
					
		if (match_em.isCompleted()) 
			bucket.put(MatchEMTable.MATCHEM_COMPLETE, 1);
		else
			bucket.put(MatchEMTable.MATCHEM_COMPLETE, 0);
		
		return database.insert(MatchEMTable.MATCHEM_T, null, bucket);
	}

	@Override
	public MatchEm getMatchEm(int the_id) {
		String matchemSelection = MatchEMTable.MATCHEM_ID + "= ?";
		String[] matchemSelectionArgs = new String[]{Integer.toString(the_id)};
		Cursor matchemCursor = database.query(MatchEMTable.MATCHEM_T, null, matchemSelection, matchemSelectionArgs, null, null, null);
		MatchEm extractedMatchem = null;
		int matchem_complete = 0;
		
		HashMap<String,String> dataHashMap = new HashMap<String, String>();
		
		if (matchemCursor.moveToFirst()) {
				extractedMatchem = new MatchEm(matchemCursor.getString(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_CATEGORY)),
						matchemCursor.getString(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_TITLE)),
						matchemCursor.getString(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_MAP)));
				extractedMatchem.setId(matchemCursor.getInt(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_ID)));
				extractedMatchem.setNextMatchEmId(matchemCursor.getInt(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_NEXT_ID)));
				matchem_complete = matchemCursor.getInt(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_COMPLETE));
				
				if (matchem_complete == 1) 
					extractedMatchem.setIsCompleted(true);
				else 
					extractedMatchem.setIsCompleted(false);
				
				try {
		        JSONObject json = new JSONObject(extractedMatchem.getMapString());
		        JSONArray names = json.names();
		        for (int i = 0; i < names.length(); i++) {
		            String key = names.getString(i);
		            dataHashMap.put(key, (String) json.opt(key));
		        }
		    } catch (JSONException e) {
		        e.printStackTrace();
			}
				extractedMatchem.setMap(dataHashMap);
		}
		return extractedMatchem;
	}


	@Override
	public ArrayList<MatchEm> getAllMatchEms(String category) {
		return extractMatchEMs(category);
	}
	
	private ArrayList<MatchEm> extractMatchEMs(String the_category) {
		ArrayList<MatchEm> matchems = new ArrayList<MatchEm>();
		String matchemSelection = MatchEMTable.MATCHEM_CATEGORY + " = ?";
		String[] matchemSelectionArgs = new String[]{the_category};
		Cursor matchemCursor = database.query(MatchEMTable.MATCHEM_T, null, matchemSelection, matchemSelectionArgs, null, null, null);
		MatchEm extractedMatchem = null;
		int matchem_complete = 0;
		
		HashMap<String,String> dataHashMap = null;
		
		if (matchemCursor.moveToFirst()) {
			for (matchemCursor.moveToFirst(); !matchemCursor.isAfterLast(); matchemCursor.moveToNext()) {
				extractedMatchem = new MatchEm(the_category, matchemCursor.getString(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_TITLE)),
						matchemCursor.getString(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_MAP)));
				extractedMatchem.setId(matchemCursor.getInt(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_ID)));
				extractedMatchem.setNextMatchEmId(matchemCursor.getInt(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_NEXT_ID)));
				matchem_complete = matchemCursor.getInt(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_COMPLETE));
				
				if (matchem_complete == 1) 
					extractedMatchem.setIsCompleted(true);
				else 
					extractedMatchem.setIsCompleted(false);
				
				try {
					dataHashMap = new HashMap<String, String>();
					JSONObject json = new JSONObject(extractedMatchem.getMapString());
					JSONArray names = json.names();
					for (int i = 0; i < names.length(); i++) {
						String key = names.getString(i);
						dataHashMap.put(key, (String) json.opt(key));
					}
				} catch (JSONException e) {
					e.printStackTrace();

				}
				extractedMatchem.setMap(dataHashMap);
				matchems.add(extractedMatchem);
			}
		}
		
		return matchems;
		}

	@Override
	public ArrayList<MatchEm> getAllMatchEms() {
		
		ArrayList<MatchEm> matchems = new ArrayList<MatchEm>();
		HashMap<String, String> dataHashMap = new HashMap<String, String>();
		Cursor matchemCursor = database.query(MatchEMTable.MATCHEM_T, null, null, null, null, null, null);
		MatchEm extractedMatchem = null;
		int matchem_complete = 0;
				
				if (matchemCursor.moveToFirst()) {
					for (matchemCursor.moveToFirst(); !matchemCursor.isAfterLast(); matchemCursor.moveToNext()) {
						extractedMatchem = new MatchEm(matchemCursor.getString(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_CATEGORY)), matchemCursor.getString(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_TITLE)),
								matchemCursor.getString(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_MAP)));
						extractedMatchem.setId(matchemCursor.getInt(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_ID)));
						extractedMatchem.setNextMatchEmId(matchemCursor.getInt(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_NEXT_ID)));
						matchem_complete = matchemCursor.getInt(matchemCursor.getColumnIndexOrThrow(MatchEMTable.MATCHEM_COMPLETE));
						
						if (matchem_complete == 1) 
							extractedMatchem.setIsCompleted(true);
						else 
							extractedMatchem.setIsCompleted(false);
						
						try {
				        JSONObject json = new JSONObject(extractedMatchem.getMapString());
				        JSONArray names = json.names();
				        for (int i = 0; i < names.length(); i++) {
				            String key = names.getString(i);
				            dataHashMap.put(key, (String) json.opt(key));
				        }
				    } catch (JSONException e) {
				        e.printStackTrace();

					}
						extractedMatchem.setMap(dataHashMap);
						matchems.add(extractedMatchem);
					}
				} 
		return matchems;
	}
	
	@Override
	public void deleteMatchEm(final MatchEm the_matchem) {
		String matchemWhereClause = MatchEMTable.MATCHEM_ID + " = ?";
		String[] matchemWhereArgs = new String[1];
		matchemWhereArgs[0] = Integer.toString(the_matchem.getID());
		database.delete(MatchEMTable.MATCHEM_T, matchemWhereClause, matchemWhereArgs);
	}
	
	private static class MyDBHelper extends SQLiteOpenHelper {
		public MyDBHelper(final Context the_context) {
			super(the_context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		@Override
		public void onCreate(SQLiteDatabase the_db) {

			the_db.execSQL("CREATE TABLE " + MatchEMTable.MATCHEM_T + " (" +
					MatchEMTable.MATCHEM_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + 
					MatchEMTable.MATCHEM_COMPLETE + " INTEGER, " + 
					MatchEMTable.MATCHEM_ID + " TEXT NOT NULL, " +
					MatchEMTable.MATCHEM_NEXT_ID + " TEXT NOT NULL, " +
					MatchEMTable.MATCHEM_TITLE + " TEXT NOT NULL, " + 
					MatchEMTable.MATCHEM_CATEGORY + " TEXT NOT NULL, " +
					MatchEMTable.MATCHEM_MAP + " TEXT NOT NULL);" 
					);
		}
		

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w("LOG_TAG", "Updgrading database from version " + oldVersion + " to " +
					newVersion + ", which will destroy all old data");
						
						//Kill previous table if upgraded
			db.execSQL("DROP TABLE IF EXISTS " + MatchEMTable.MATCHEM_T);
						
						//Create new instance of schema
						onCreate(db);
		}	
	}
}


