package com.inspirethis.mike.matchterms2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

//import com.example.matchem.R;

/**
 * This Activity allows User to build their own Match Term List
 * @author Michael Jagielo
 *
 */
public class CreateMatchEmsActivity extends Activity {

	private EditText name, key_1, val_1, key_2, val_2, key_3, val_3, key_4, val_4, key_5, val_5;
	private Button submitButton; 
	private MatchEm new_matchem;
	private TextView textView_1, textView_2, textView_3, textView_4, textView_5, 
	textView_6, textView_7, textView_8, textView_9, textView_10;
	private SharedPreferences settings;
	private final MatchEmDBAdapter database = MatchEmDBAdapter.getInstance(CreateMatchEmsActivity.this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_match_ems);
		addListenerOnSubmitButton();
		setUpEditTextBoxes();
		setUpTextViews();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_match_ems, menu);
		return true;
	}
	
	private void setUpEditTextBoxes() {
		name = (EditText) findViewById(R.id.enter_name);
		
		key_1 = (EditText) findViewById(R.id.enter_key_1);
		val_1 = (EditText) findViewById(R.id.enter_val_1);
		key_2 = (EditText) findViewById(R.id.enter_key_2);
		val_2 = (EditText) findViewById(R.id.enter_val_2);
		key_3 = (EditText) findViewById(R.id.enter_key_3);
		val_3 = (EditText) findViewById(R.id.enter_val_3);
		key_4 = (EditText) findViewById(R.id.enter_key_4);
		val_4 = (EditText) findViewById(R.id.enter_val_4);
		key_5 = (EditText) findViewById(R.id.enter_key_5);
		val_5 = (EditText) findViewById(R.id.enter_val_5);
		key_1.addTextChangedListener(mTextEditorWatcher_1);
		val_1.addTextChangedListener(mTextEditorWatcher_2);
		key_2.addTextChangedListener(mTextEditorWatcher_3);
		val_2.addTextChangedListener(mTextEditorWatcher_4);
		key_3.addTextChangedListener(mTextEditorWatcher_5);
		val_3.addTextChangedListener(mTextEditorWatcher_6);
		key_4.addTextChangedListener(mTextEditorWatcher_7);
		val_4.addTextChangedListener(mTextEditorWatcher_8);
		key_5.addTextChangedListener(mTextEditorWatcher_9);
		val_5.addTextChangedListener(mTextEditorWatcher_10);
	}
	
	private void setUpTextViews() {
		textView_1 = (TextView) findViewById(R.id.text_length_1);
		textView_1.setPadding(2, 2, 0, 0);
		textView_2 = (TextView) findViewById(R.id.text_length_2);
		textView_2.setPadding(2, 2, 0, 0);
		textView_3 = (TextView) findViewById(R.id.text_length_3);
		textView_3.setPadding(2, 2, 0, 0);
		textView_4 = (TextView) findViewById(R.id.text_length_4);
		textView_4.setPadding(2, 2, 0, 0);
		textView_5 = (TextView) findViewById(R.id.text_length_5);
		textView_5.setPadding(2, 2, 0, 0);
		textView_6 = (TextView) findViewById(R.id.text_length_6);
		textView_6.setPadding(2, 2, 0, 0);
		textView_7 = (TextView) findViewById(R.id.text_length_7);
		textView_7.setPadding(2, 2, 0, 0);
		textView_8 = (TextView) findViewById(R.id.text_length_8);
		textView_8.setPadding(2, 2, 0, 0);
		textView_9 = (TextView) findViewById(R.id.text_length_9);
		textView_9.setPadding(2, 2, 0, 0);
		textView_10 = (TextView) findViewById(R.id.text_length_10);
		textView_10.setPadding(2, 2, 0, 0);
	}
	
	private void clearEditText() {
		key_1.getText().clear();
		val_1.getText().clear();
		key_2.getText().clear();
		val_2.getText().clear();
		key_3.getText().clear();
		val_3.getText().clear();
		key_4.getText().clear();
		val_4.getText().clear();
		key_5.getText().clear();
		val_5.getText().clear();
	}
	
	private void addListenerOnSubmitButton() {
		submitButton = (Button) findViewById(R.id.submit_button);
		submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (checkTextBoxes() && checkTextLength()){
					String temp_name = name.getText().toString();
					
					String[] str = new String[10];
					str[0] = val_1.getText().toString();
					str[1] = key_1.getText().toString();
					str[2] = val_2.getText().toString();
					str[3] = key_2.getText().toString();
					str[4] = val_3.getText().toString();
					str[5] = key_3.getText().toString();
					str[6] = val_4.getText().toString();
					str[7] = key_4.getText().toString();
					str[8] = val_5.getText().toString();
					str[9] = key_5.getText().toString();
					
					new_matchem = buildMatchEM(UserBuiltMatchEms.getName(), temp_name, str);

					UserBuiltMatchEms.addMatchEM(new_matchem);
					database.open();
					database.addMatchEm(new_matchem);
					database.close();

					// set boolean key in sharedPreferences true as a MatchEm now exists in database.
			        settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
			        Editor editor = settings.edit();
			        editor.putBoolean("boolKeyAdded", true); 
			        editor.commit();

					startActivity(new Intent(CreateMatchEmsActivity.this, MainActivity.class));
					finish();
				} else if (!checkTextBoxes()){
					Toast.makeText(CreateMatchEmsActivity.this,
							"Please complete text boxes...",Toast.LENGTH_SHORT).show();
				} else if (!checkTextLength()) {
					Toast.makeText(CreateMatchEmsActivity.this,
							"Please check the number of characters in text boxes...",Toast.LENGTH_SHORT).show();
				}
			} 
			
			private boolean checkTextBoxes() {
				return ((name.getText().toString().length() > 0) &&
						(key_1.getText().toString().length() > 0) &&
						(val_1.getText().toString().length() > 0)&&
						(key_2.getText().toString().length() > 0) &&
						(val_2.getText().toString().length() > 0) &&
						(key_3.getText().toString().length() > 0) &&
						(val_3.getText().toString().length() > 0) &&
						(key_4.getText().toString().length() > 0) &&
						(val_4.getText().toString().length() > 0) &&
						(key_5.getText().toString().length() > 0) &&
						(val_5.getText().toString().length() > 0));
			}
			
			private boolean checkTextLength() {
				return (!(key_1.getText().toString().length() > 20) &&
						!(val_1.getText().toString().length() > 80)&&
						!(key_2.getText().toString().length() > 20) &&
						!(val_2.getText().toString().length() > 80) &&
						!(key_3.getText().toString().length() > 20) &&
						!(val_3.getText().toString().length() > 80) &&
						!(key_4.getText().toString().length() > 20) &&
						!(val_4.getText().toString().length() > 80) &&
						!(key_5.getText().toString().length() > 20) &&
						!(val_5.getText().toString().length() > 80));
			}

		});
	}
	
    public MatchEm buildMatchEM(String cat, String name, String[] key_val) {
    	MatchEm matchem;
    	
    	HashMap<String, String> map = new HashMap<String, String>();
    	map.put(key_val[0], key_val[1]);
    	map.put(key_val[2], key_val[3]);
    	map.put(key_val[4], key_val[5]);
    	map.put(key_val[6], key_val[7]);
    	map.put(key_val[8], key_val[9]);
    	
    	matchem = new MatchEm(cat, name, map);
    	
    	return matchem;        	
    }
    
	// set TextWatcher for each textView
	private final TextWatcher mTextEditorWatcher_1 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           //This sets a textView to the current length
           textView_1.setText("Total Characters: " + String.valueOf(s.length()) + "    Max: 20");
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	private final TextWatcher mTextEditorWatcher_2 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           textView_2.setText("Total Characters: " + String.valueOf(s.length())+ "    Max: 80");
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	private final TextWatcher mTextEditorWatcher_3 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           textView_3.setText("Total Characters: " + String.valueOf(s.length())+ "    Max: 20");
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	private final TextWatcher mTextEditorWatcher_4 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           textView_4.setText("Total Characters: " + String.valueOf(s.length())+ "    Max: 80");
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	private final TextWatcher mTextEditorWatcher_5 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           textView_5.setText("Total Characters: " + String.valueOf(s.length())+ "    Max: 20");
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	private final TextWatcher mTextEditorWatcher_6 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           textView_6.setText("Total Characters: " + String.valueOf(s.length())+ "    Max: 80");
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	private final TextWatcher mTextEditorWatcher_7 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           textView_7.setText("Total Characters: " + String.valueOf(s.length())+ "    Max: 20");
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	private final TextWatcher mTextEditorWatcher_8 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           textView_8.setText("Total Characters: " + String.valueOf(s.length())+ "    Max: 80");
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	private final TextWatcher mTextEditorWatcher_9 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           textView_9.setText("Total Characters: " + String.valueOf(s.length())+ "    Max: 20");
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	private final TextWatcher mTextEditorWatcher_10 = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           textView_10.setText("Total Characters: " + String.valueOf(s.length())+ "    Max: 80");
        }

        public void afterTextChanged(Editable s) {
        }
	};

	@Override
	public void onBackPressed() {
		clearEditText();
		startActivity(new Intent(CreateMatchEmsActivity.this, MainActivity.class));
		finish();
	}











}
