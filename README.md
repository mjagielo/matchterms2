**README**

Quick summary:

Match Terms is being built for a fun matching game and to act as a study guide allowing users the ability to create their Match Term Lists dynamically as well as download new content from server. 
 

This current iteration includes server connection via **Google AppEngine Cloud Endpoints**.

Recently updated to test Android M Preview: https://developer.android.com/preview/setup-sdk.html#get-as13 compileSdkVersion 'android-MNC'; minSdkVersion && targetSdkVersion 'MNC'
Android Studio 1.3, Canary Channel; Android SDK: Preview Channel Android Platform: Android MNC Preview

TODO: 
Update to 5.1.1
AdMob
Release Google Play Store